import * as config from "./config";
let activeUsers = [];

let rooms = [{
    name: 'room1',
    users: []
}];
//[{name: roomName, users: {name: '123', status: 'READY', progress: ''}}]


export default io => {
    io.on("connection", socket => {
            const username = socket.handshake.query.username;
            socket.emit("UPDATE_ROOMS", rooms);

            // activate user if username is unique
            if (activeUsers.includes(username)) {
                socket.emit("DEACTIVATE_USER");
                socket.disconnect();
            } else {
                activeUsers.push(username);
                console.log("ACTIVE USERs:", activeUsers);

            }

            // add a room
            socket.on("ADD_ROOM", (name) => {
                const roomExists = rooms.find(room => {
                    return room?.name === name
                })
                if (!roomExists) {
                    rooms.push({
                        name,
                        users: [{name: username, status: 'NOT_READY', progress: 0}]
                    })
                    // Do we need this here???
                    socket.broadcast.emit("UPDATE_ROOMS", rooms);
                    socket.emit("UPDATE_ROOMS", rooms);
                } else {
                    socket.emit("ADD_ROOM_ERROR");
                }
            });

            //add user to a room
            socket.on("JOIN_ROOM", (roomName) => {
                    //check if user is already in this room
                    const currentRoom = rooms.find(room => {
                        return room.name === roomName
                    });
                    const userIndex = currentRoom?.users.findIndex(user => user.name === username);
                    if (userIndex === -1 && currentRoom?.users.length === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
                        socket.emit("JOIN_ROOM_ERROR");
                    } else if (userIndex === -1 && currentRoom?.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
                        rooms.find(room => {
                            return room.name === roomName
                        }).users.push({name: username, status: 'NOT_READY', progress: 0});

                    } else {
                        const currentRoom = rooms.find(room => {
                            return room.name === roomName
                        });
                        socket.join(roomName, () => {
                            io.to(socket.id).emit("JOIN_ROOM_DONE", {
                                currentRoom,
                                roomName
                            });
                        });
                        socket.emit("UPDATE_ROOM", currentRoom);

                    };


                    socket.broadcast.emit("UPDATE_ROOMS", rooms);
                    socket.emit("UPDATE_ROOMS", rooms);
            }
        );
        // on quit room 
        socket.on("QUIT_ROOM", (roomName) => {
            //remove user from current room
            const currentRoom = rooms.find(room => {
                return room.name === roomName
            });
            console.log("CURRENT ROOM:", currentRoom);
            currentRoom.users = currentRoom?.users?.filter(user => user.name !== username);
            // Delete room with no users
            if(!currentRoom?.users.length){
              rooms=rooms.filter(room=> room.name!==roomName);
            }
            console.log(socket.id);
            debugger;
            socket.leave(roomName);
            debugger;
            // socket.broadcast.emit("UPDATE_ROOMS", rooms);
            socket.emit("UPDATE_ROOMS", rooms);
            // socket.broadcast.emit("UPDATE_ROOM", currentRoom);
            socket.emit("UPDATE_ROOM", currentRoom);
        });

        // on change ready status
        socket.on("CHANGE_READY_STATUS", (roomName) => {
            //remove user from current room
            console.log("ROOM:", rooms);
            console.log("ROOM NAME:", roomName);
            const currentRoom = rooms.find(room => {
                return room.name === roomName
            });
         
            console.log("CURRENT ROOM:", currentRoom);
            const currentStatus = currentRoom.users.find(user => user.name === username).status;
            console.log("CURRENT STATUS:", currentStatus);
            currentRoom.users.find(user => user.name === username).status =  currentStatus==='READY' ? 'NOT_READY' : 'READY';
            socket.emit("UPDATE_ROOM", currentRoom);
            // io.to(roomName).emit("UPDATE_COUNTER", currentRoom);
        });


        socket.on("disconnect", () => {
            console.log(`${username} disconnected`);
            activeUsers = activeUsers.filter(user => user !== username);
        });

    });



};