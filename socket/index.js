import * as config from "./config";
import { texts } from './../data';
let users = [];
//users=[{name: '123', status: 'READY', progress: ''},...]

const startGame = (socket) => {
    const randomIndex = Math.floor(Math.random() * texts.length);
    const text = texts[randomIndex];
    socket.emit("SHOW_TEXT", text);
    socket.broadcast.emit("SHOW_TEXT", text);
  }

export default io => {
    io.on("connection", socket => {
            const username = socket.handshake.query.username;

            // activate user if username is unique
            if (users.find(user=>user.name===username)) {
                socket.emit("DEACTIVATE_USER");
                socket.disconnect();
            } else {
                users.push({name: username, status: 'NOT_READY', progress: 0});
                socket.emit("UPDATE_GAME", users);
                socket.broadcast.emit("UPDATE_GAME", users);
            }

        // on change ready status
        socket.on("TOGGLE_READY_STATUS", () => {
            //remove user from current room
            const currentUser = users.find(user=>user.name===username);
            const {status} = currentUser;
         
            users.find(user => user.name === username).status =  status==='READY' ? 'NOT_READY' : 'READY';

            //check if all users are active
            const notReadyUsers = users.filter(user=>user.status==='NOT_READY');
            if(!notReadyUsers.length){
                console.log("GAME START")
                socket.emit("START_GAME");
                socket.broadcast.emit("START_GAME");

                let counter = config.SECONDS_TIMER_BEFORE_START_GAME;
                const countdown = setInterval(()=>{
                  io.sockets.emit('UPDATE_COUNTDOWN', counter);
                  counter--;
                  if (counter === -1) {
                    clearInterval(countdown);
                    startGame(socket);
                  }
                }, 1000);
            }
            socket.emit("UPDATE_GAME", users);
            socket.broadcast.emit("UPDATE_GAME", users);
        });

        socket.on("UPDATE_USER_PROGRESS", (progress)=>{
            users.find(user => user.name === username).progress =  progress;
            socket.emit("UPDATE_GAME", users);
            socket.broadcast.emit("UPDATE_GAME", users);

        });

        socket.on("GAME_OVER", ()=>{
            const results = users.sort((a, b) =>{
                return a.progress - b.progress;
            });
            socket.broadcast.emit("GAME_OVER_DONE", results);
            socket.emit("GAME_OVER_DONE", results);
        });


        socket.on("disconnect", () => {
            console.log(`${username} disconnected`);
           users= users.filter(user => user.name !== username);
        });

    });

};