import { createElement, addClass, removeClass } from "./helper.mjs";

const username = sessionStorage.getItem("username");

const roomsPageContainer = document.getElementById("rooms-page");
const gamePageContainer = document.getElementById("game-page");


if (!username) {
  window.location.replace("/login");
}

const deactivateUser = () =>{
  alert("User with such username already exists");
  sessionStorage.removeItem("username");
  window.location.replace("/login");
}

const createUserItem = (user) => {
  const userItem = createElement({
    tagName: "div",
    className: "user-item",
  });

  const userInfo = createElement({
    tagName: "div",
    className: "user-info",
  });

  const userStatus = createElement({
    tagName: "div",
    className: "ready-status",
    attributes: { id: `ready-status-${user.name}` }
  })

  addClass(userStatus, `${user.status==='READY' ? 'ready-status-green': 'ready-status-red'}`);

  const userName = createElement({
    tagName: "span",
    className: "",
  })

  userName.innerHTML= `${user.name} ${user.name===username ? '(you)' : ''}`

  userInfo.appendChild(userStatus);
  userInfo.appendChild(userName);

  const userProgress = createElement({
    tagName: "progress",
    className: "user-progress",
    attributes: { max: 100, value: user.progress }
  });
  

  userItem.appendChild(userInfo);
  userItem.appendChild(userProgress);

  return userItem;
};

const toggleReadyStatus = () =>{
  socket.emit("TOGGLE_READY_STATUS");
}


const renderUsers =(users)=>{
  const usersContainer = document.getElementById('users-container');
  const readyBtn = document.getElementById('ready-btn');

  if(readyBtn){
  readyBtn.innerHTML = users.find(user=>user.name===username).status==='READY' ? 'Not Ready' : 'Ready';
  readyBtn.addEventListener('click', toggleReadyStatus)
  }

  const allUsers = users.map(createUserItem);
  usersContainer.innerHTML = "";
  usersContainer.append(...allUsers);
}


const startGame = ()=>{
  const readyBtn = document.getElementById('ready-btn');
  addClass(readyBtn, 'display-none');
}

const updateCountdown = (counter) =>{
  const textContainer = document.getElementById('text-container');
  textContainer.innerHTML = counter;
}

const getChar = (event)=> {
  if (event.which == null) {
    if (event.keyCode < 32) return null;
    return String.fromCharCode(event.keyCode)
  }

  if (event.which != 0 && event.charCode != 0) {
    if (event.which < 32) return null;
    return String.fromCharCode(event.which);
  }

  return null;
}



const highlight = (text, counter)=> {
  var textContainer = document.getElementById("text-container");
  if (counter > 0) { 
   const updatedContent = "<span class='highlight'>"+text.substring(0,counter) + "</span>" + "<span class='underline'>"+text.substring(counter, counter+1)+"</span>"+text.substring(counter+1, counter + text.length);
   textContainer.innerHTML = updatedContent;
  }
}



const showText = (text) =>{
  const textContainer = document.getElementById('text-container');
  textContainer.innerHTML = text;

  let counter = 0;

const keyPressListener = (event, text)=> {
  const inputSymbol = getChar(event);
  if(inputSymbol===text[counter]){
    counter++;
    highlight(text, counter);
    socket.emit("UPDATE_USER_PROGRESS", parseInt(counter/text.length*100));
  }
  if(counter===text.length)
  socket.emit("GAME_OVER")
}


  window.addEventListener('keypress', (event)=>{keyPressListener(event, text)});
}


const gameOver =(results)=>{
  alert(results);
}

const socket = io("", { query: { username } });
socket.emit("ACTIVATE_USER", username);
socket.on("UPDATE_GAME", renderUsers);
socket.on("DEACTIVATE_USER", deactivateUser);
socket.on("START_GAME", startGame);
socket.on("UPDATE_COUNTDOWN", updateCountdown);
socket.on("SHOW_TEXT", showText);
socket.on("GAME_OVER_DONE", gameOver);