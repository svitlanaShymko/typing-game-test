import path from "path";

export const STATIC_PATH = path.join(__dirname, "public");
export const HTML_FILES_PATH = path.join(STATIC_PATH, "html");
export const TEXT_DATA_PATH = path.join(STATIC_PATH, "data");

export const PORT = 3002;
